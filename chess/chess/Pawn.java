package chess;

public class Pawn extends Piece {
    public Pawn(PieceColour pieceColour) {
        this.pieceColour = pieceColour;                                                 // Pawn class inherits behaviour of piece
        if (pieceColour == PieceColour.BLACK) {
            setSymbol("♟");
        } else {
            setSymbol("♙");
        }
    }

    @Override
    public boolean isLegitMove(int oldRow, int oldColumn, int newRow, int newColumn) {
        if ((oldRow == newRow) && (oldColumn == newColumn)) {
            return false;
        }                                                                               // If user enters same spot return false

        Piece fromPiece = Board.getBoard()[oldRow][oldColumn].getPiece();
        Piece toPiece = Board.getBoard()[newRow][newColumn].getPiece();

        if (fromPiece == null){
            System.out.println("Invalid Move - Cannot move empty piece");
            return false;
        }

        if (toPiece != null) {
            if (fromPiece.getColour() == toPiece.getColour()) {
                System.out.println("Invalid Move - cannot place over friendly pieces");
                return false;
            }
            else{
                if(fromPiece.getColour()==pieceColour.BLACK){
                    if (newRow-oldRow == 1 && Math.abs(newColumn-oldColumn)==1 ){
                        this.updateCoordinates(newRow,newColumn);
                        return true;}}
                else {if (newRow-oldRow == -1 && Math.abs(newColumn-oldColumn)==1 ){
                    this.updateCoordinates(newRow,newColumn);
                    return true;}}                                                      //Ensures Pawn can only capture diagonally

            }
            return false;}

        // This is for normal pawn moves.
        if (Math.abs(oldRow - newRow) == 1
                && Math.abs(oldColumn - newColumn) == 0) {                                    // If change in x is 0 and change in y 1 then
            // White can only move down
            if (this.getColour() == pieceColour.WHITE) {                                // if white piece, then if old y > new y return true
                if (oldRow > newRow) {
                    this.updateCoordinates(newRow,newColumn);
                    return true;
                }
            }
            // Black can only move forward.
            if (this.getColour() == pieceColour.BLACK) {
                if (oldRow < newRow) {
                    this.updateCoordinates(newRow,newColumn);
                    return true;                                                        // Ensures black piece can move 1 space forward (down the board)
                }
            }

        }

        if (Math.abs(oldRow - newRow) == 2                                        // Ensure can move 2 ahead on first turn
                && Math.abs(oldColumn - newColumn) == 0
                && (oldRow == 1 || oldRow == 6)) {

            // White can only move down
            if (this.getColour() == pieceColour.WHITE) {                                // if white piece, then if old y > new y return true
                if (oldRow > newRow) {
                    this.updateCoordinates(newRow,newColumn);
                    return true;
                }
            }
            // Black can only move forward.
            if (this.getColour() == pieceColour.BLACK) {
                if (oldRow < newRow) {
                    this.updateCoordinates(newRow,newColumn);
                    return true;                                                        // Ensures black piece can move 2 space forward (down board)
                }
            }

        }
            return false;

        }
    }


// piece moves 2 first go and 1 every oother direction up