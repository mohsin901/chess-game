package chess;

public class CheckInput {
	
	
	public boolean checkCoordinateValidity(String input){
		if (input.length()!=2){									//Check if the length of the string is 2
			return false;
		}

		input = input.toLowerCase();
		return input.matches("^[1-8][a-h]");				//Check if input is in correct format

	}
}
