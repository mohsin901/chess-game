package chess;

public class Rook extends Piece {
    public Rook(PieceColour pieceColour) { this.pieceColour = pieceColour;              //Rook class inherits behaviour of piece
        if (pieceColour == PieceColour.BLACK) {
            setSymbol("♜");
        } else {
            setSymbol("♖");
        }
    }


    @Override
    public boolean isLegitMove(int oldRow, int oldColumn, int newRow, int newColumn) {
        if ((oldRow==newRow) && (oldColumn == newColumn) ){
            return false;
        }                                                                               //If user enters same spot return false



        Piece fromPiece = Board.getBoard()[oldRow][oldColumn].getPiece();
        Piece toPiece = Board.getBoard()[newRow][newColumn].getPiece();

        int differenceInRows = Math.abs(oldRow - newRow);

        for (int j = 1; j < differenceInRows; j++) {
            if ((newRow < oldRow) && (newColumn > oldColumn)
                    && Board.getBoard()[oldRow - j][oldColumn + j].hasPiece()) {
                return false;
            } else if ((newRow > oldRow) && (newColumn > oldColumn)
                    && Board.getBoard()[oldRow + j][oldColumn + j].hasPiece()) {
                return false;
            } else if ((newRow > oldRow) && (newColumn < oldColumn)
                    && Board.getBoard()[oldRow + j][oldColumn - j].hasPiece()) {
                return false;
            } else if ((newRow < oldRow) && (newColumn < oldColumn)
                    && Board.getBoard()[oldRow - j][oldColumn - j] .hasPiece()) {    //Check if piece is blocked
                return false;
            }

        }

        if (fromPiece == null){
            System.out.println("Invalid Move - Cannot move empty piece");
            return false;
        }

        if (toPiece != null) {
            if (fromPiece.getColour() == toPiece.getColour()) {
                System.out.println("Invalid Move - cannot place over friendly pieces");
                return false;
            }
        }

        if (newRow == oldRow)
            return true;                                                                //Ensures pieces can move left or right any no of steps
            this.updateCoordinates(newRow,newColumn);
        if (newColumn == oldColumn)
            return true;                                                                //Ensures pieces can move up or down any no of steps
            this.updateCoordinates(newRow,newColumn);

        return false;
    }
}


