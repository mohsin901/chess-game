package chess;

import java.io.Console;
import java.util.Scanner;
import static chess.Board.pWin;

public class Game {
    public static boolean playerTurn = true;
    private static boolean gameEnd = false;
    public static Board b = new Board();

    public Game() {

        CheckInput c = new CheckInput();
        b.initialisePieces();
        b.printBoard();
        while (!gameEnd) {

            //write the game logic
            Scanner myObj = new Scanner(System.in);
            if (playerTurn){
				System.out.println("------ White Player Turn ------");
			}
            else{
				System.out.println("------ Black Player Turn ------");}

            System.out.println(">Enter Origin");
            String inputOrigin = myObj.nextLine();

            if (c.checkCoordinateValidity(inputOrigin)) {
                System.out.println(">Enter Destination");                                                        // else continue
                String inputDestination = myObj.nextLine();
                if (c.checkCoordinateValidity(inputDestination)) {
                    Tuple<Integer, Integer> from = getCoordinatePairFromInput(inputOrigin);
                    Tuple<Integer, Integer> to = getCoordinatePairFromInput(inputDestination);
                    Piece fromPiece = b.getPiece(from.getFirst(), from.getSecond());
                    Piece toPiece = b.getPiece(to.getFirst(), to.getSecond());

                    if (fromPiece == null){
                        System.out.println("Invalid Move - Cannot move empty piece");
                        continue;
                    }

                    if (toPiece!=null){
                        if (fromPiece.getColour() == toPiece.getColour()) {
                            System.out.println("Invalid Move - cannot place over friendly pieces");
                            continue;
                        }
                    }

                    if (playerTurn && fromPiece.getColour() != PieceColour.WHITE || !playerTurn && fromPiece.getColour() != PieceColour.BLACK) {
                        System.out.println("Invalid Move");

                    }
                    else {
                        b.movePiece(from.getFirst(), from.getSecond(), to.getFirst(), to.getSecond(), fromPiece);
                        b.printBoard();

                        if(pWin){
                            if(playerTurn){
                                System.out.println("White player wins");
                            }
                            else {
                                System.out.println("Black Player wins");
                            }
                            gameEnd = true;
                        }
                        }
                        }


                    }



            }

        }

    public static Tuple<Integer, Integer> getCoordinatePairFromInput(String input) {
        int firstDigit = Integer.parseInt(input.charAt(0) + "");
        firstDigit = firstDigit - 1;

        char secondDigit = input.charAt(1);
        int newSecondDigit = 0;
        if (secondDigit == 'a') {
            newSecondDigit = 0;
        } else if (secondDigit == 'b') {
            newSecondDigit = 1;
        } else if (secondDigit == 'c') {
            newSecondDigit = 2;
        } else if (secondDigit == 'd') {
            newSecondDigit = 3;
        } else if (secondDigit == 'e') {
            newSecondDigit = 4;
        } else if (secondDigit == 'f') {
            newSecondDigit = 5;
        } else if (secondDigit == 'g') {
            newSecondDigit = 6;
        } else if (secondDigit == 'h') {
            newSecondDigit = 7;
        }

        return new Tuple<Integer, Integer>(firstDigit, newSecondDigit);
    }

    public static void main(String args[]) {
        Game g = new Game();
    }
}
