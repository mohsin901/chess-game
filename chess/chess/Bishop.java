package chess;

public class Bishop extends Piece {


    public Bishop(PieceColour pieceColour) {                                             //Bishop class inherits behaviour of piece
        this.pieceColour = pieceColour;
        if (pieceColour == PieceColour.BLACK) {
            setSymbol("♝");
        } else {
            setSymbol("♗");
        }

    }

    @Override
    public boolean isLegitMove(int oldRow, int oldColumn, int newRow, int newColumn) {


        if ((oldRow == newRow) && (oldColumn == newColumn)) {
            return false;
        }                                                                               //If user enters same spot return false


        Piece fromPiece = Board.getBoard()[oldRow][oldColumn].getPiece();
        Piece toPiece = Board.getBoard()[newRow][newColumn].getPiece();

        int differenceInRows = Math.abs(oldRow - newRow);

        for (int j = 1; j < differenceInRows; j++) {
            if ((newRow < oldRow) && (newColumn > oldColumn)
                    && Board.getBoard()[oldRow - j][oldColumn + j].hasPiece()) {
                return false;
            } else if ((newRow > oldRow) && (newColumn > oldColumn)
                    && Board.getBoard()[oldRow + j][oldColumn + j].hasPiece()) {
                return false;
            } else if ((newRow > oldRow) && (newColumn < oldColumn)
                    && Board.getBoard()[oldRow + j][oldColumn - j].hasPiece()) {
                return false;
            } else if ((newRow < oldRow) && (newColumn < oldColumn)
                    && Board.getBoard()[oldRow - j][oldColumn - j] .hasPiece()) {   //Check if blocked
                return false;
            }

        }


        if (fromPiece == null){
            System.out.println("Invalid Move - Cannot move empty piece");
            return false;
        }

        if (toPiece != null) {
            if (fromPiece.getColour() == toPiece.getColour()) {
                System.out.println("Invalid Move - cannot place over friendly pieces");
                return false;
            }

        }

        int Diff_x = Math.abs(newColumn - oldColumn);
        int Diff_y = Math.abs(newRow - oldRow);
        if (Diff_x == Diff_y) {
            this.updateCoordinates(newRow, newColumn);              //Bishop can move diagonally any no of steps
            return true;
        }
        return false;
    }}





                                                                      //Ensures pieces can move only diagonally


///////////NEED TO CHECK IF FRIENDLY PIECES IN THE WAY

