package chess;

public class Knight extends Piece {
    public Knight(PieceColour pieceColour) {
        this.pieceColour = pieceColour;                                                 //Knight class inherits behaviour of piece
        if (pieceColour == PieceColour.BLACK) {
            setSymbol("♞");
        } else {
            setSymbol("♘");
        }
    }

    @Override
    public boolean isLegitMove(int oldRow, int oldColumn, int newRow, int newColumn) {
        if ((oldRow==newRow) && (oldColumn == newColumn) ){
            return false;
        }                                                                               //If user enters same spot return false

        Piece fromPiece = Board.getBoard()[oldRow][oldColumn].getPiece();
        Piece toPiece = Board.getBoard()[newRow][newColumn].getPiece();


        if (fromPiece == null) {
            System.out.println("Invalid Move - Cannot move empty piece");
            return false;
        }

        if (toPiece != null) {
            if (fromPiece.getColour() == toPiece.getColour()) {
                System.out.println("Invalid Move - cannot place over friendly pieces");
                return false;
            }
        }

        if(newRow != oldRow - 1 && newRow != oldRow + 1 && newRow != oldRow + 2 && newRow != oldRow - 2)
            return false;
        if(newColumn != oldColumn - 2 && newColumn != oldColumn + 2 && newColumn != oldColumn - 1 && newColumn != oldColumn + 1)
            return false;
       int diffX = Math.abs(newColumn-oldColumn);
       int diffY = Math.abs(newRow-oldRow);
       if (diffX+diffY>=4){                                                                                         //Ensure that the knight can only move in an L pattern (2 moves then 1 turn)
           return false;
       }


        this.updateCoordinates(newRow,newColumn);
        return true;

    }
}

