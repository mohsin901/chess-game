package chess;

public class Square {
    private int i;
    private int j;
    private boolean hasPiece;
    private Piece piece;

    public Square(int iIn, int jIn) {
        i = iIn;
        j = jIn;
        piece = null;
    }

	public Piece getPiece() {
		return piece;
	}

	public void setPiece(Piece piece) {
		this.piece = piece;
		hasPiece = true;
	}

	public void removePiece() {
    	this.piece = null;
    	hasPiece = false;
	}

	public boolean hasPiece() {
        return hasPiece;
    }

	public int getI() {
		return i;
	}

	public int getJ() {
		return j;
	}
}