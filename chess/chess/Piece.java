package chess;

public abstract class Piece {
    private int row;
    private int column;
    private String symbol;
    protected PieceColour pieceColour;

    public Piece() { }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public PieceColour getColour() {
        return pieceColour;
    }

    public void updateCoordinates(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public abstract boolean isLegitMove(int oldRow, int oldColumn, int newRow, int newColumn);      //Abstract method so has no code
}