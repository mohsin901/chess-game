package chess;

public class King extends Piece {
    public King(PieceColour pieceColour) { this.pieceColour = pieceColour;              //Knight class inherits behaviour of piece
        this.pieceColour = pieceColour;
        if (pieceColour == PieceColour.BLACK) {
            setSymbol("♚");
        } else {
            setSymbol("♔");
        }                                                                               //Print symbols
    }


    @Override
    public boolean isLegitMove(int oldRow, int oldColumn, int newRow, int newColumn) {
        if ((oldRow == newRow) && (oldColumn == newColumn)) {
            return false;
        }                                                                               //If user enters same spot return false

        Piece fromPiece = Board.getBoard()[oldRow][oldColumn].getPiece();
        Piece toPiece = Board.getBoard()[newRow][newColumn].getPiece();


        if (fromPiece == null) {
            System.out.println("Invalid Move - Cannot move empty piece");
            return false;
        }

        if (toPiece != null) {
            if (fromPiece.getColour() == toPiece.getColour()) {
                System.out.println("Invalid Move - cannot place over friendly pieces");
                return false;
            }
        }

        //Ensures king can move 1 step in any direction
        if (Math.abs(oldRow - newRow) == 1 && Math.abs(oldColumn - newColumn) == 0) {
            this.updateCoordinates(newRow, newColumn);
            return true;
        }

        if (Math.abs(oldColumn - newColumn) == 1 && Math.abs(oldRow - newRow) == 0) {
            this.updateCoordinates(newRow, newColumn);
            return true;
        }
        return true;
    }
}